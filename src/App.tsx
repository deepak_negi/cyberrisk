import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/Layouts/Header/NavMenu";
// import { Tabs } from 'react-bootstrap';
import Tabs from "./components/Risks/Tabs/Tabs";


class App extends React.Component<{}, {}> {
	constructor(props: any){
		super(props);
	}

	render() {
		return (
            <>
				<div className="App">
					<Header/>
					<Tabs />
					
				</div>
			</>
		);
	}
}

export default App;