import * as React from "react";
import LogoImage from "../../../assets/images/logo.png";
import UserImage from "../../../assets/images/user.png";
import SignoutImage from "../../../assets/images/sign-out-regular.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle, faChevronCircleDown, faChevronCircleUp, faClock, faSave, faTimesCircle, faHistory, faEye } from "@fortawesome/free-solid-svg-icons";


export interface NavMenuProps {

}

export interface NavMenuState {

}

class NavMenu extends React.PureComponent<NavMenuProps, NavMenuState> {
    render() {
        return (
            <div className="container-fluid">
                <header className="header row shadow-sm">
                    <div className="col-4 ">
                    <div className="h-100 logo-head d-flex justify-content-start align-items-center px-4">
                        <a href="#">
                        <img src={LogoImage} width="108" height="27" />
                        </a>
                    </div>
                    </div>
                    <div className="col-8">
                    <div className="row h-100 ">
                        <div className="offset-7 col-4 user-wrap ">
                        <div className="d-flex justify-content-end align-items-center h-100">
                            <div className="user-thumb">
                                <img className="int-width" src={UserImage} width="17" height="18" alt="" title="" />
                            </div>
                            <div className="px-3">
                            <span className="d-block medium-f">Surabhi Gupta</span>
                            <small className="text-light">Admin <FontAwesomeIcon
                            icon={faHistory}
                            className={"ml-1 text-dark"}
                            /></small>
                            </div>
                        </div>
                        </div>
                        <div className="col-1 d-flex justify-content-center align-items-center">
                        <div className="user-setting">
                            <button className="btn-trans">
                            <img src={SignoutImage} width="23px" height="" />
                            </button>
                        </div>
                        </div>
                    </div>
                    </div>
                </header>
            </div>
        )
    }
}

export default NavMenu;