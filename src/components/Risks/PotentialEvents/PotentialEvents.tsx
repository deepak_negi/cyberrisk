import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from "react-tooltip";
import Questionnaire from "../Questionnaire/Questionnaire";

interface PotentialEventsProps {
    
}
 
interface PotentialEventsState {
    
}
 
class PotentialEvents extends React.Component<PotentialEventsProps, PotentialEventsState> {
    constructor(props: PotentialEventsProps){
        super(props);
    }
    render() { 
        return (
        <div className="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div className="row mx-0">
                <div className="col-12 mt-4 ">
                    <div className="pb-1 mb-3 border-b d-flex justify-content-between">
                    <h5 className="risk-profile mb-0">RISK PROFILE QUESTIONNAIRE <FontAwesomeIcon data-tip data-for="information" icon={faInfoCircle} className="quesInfo"/>
                        
                    </h5>
                    <ReactTooltip 
                        id="information"
                        type="light"
                        place="bottom"
                        effect="solid"
                        border={true}
                        borderColor="grey"
                    >
                        <div className="popover-custome ques-block">
                            <div className="row">
                                <div className="col-3 px-2 text-center">
                                <p className="d-block">Rare <br />
                                    <span className="text-light"> (Occurs less than
                                    once every few years)</span>
                                </p>
                                <button className="risk-btn very-low-risk">Very Low</button>
                                </div>
                                <div className="col-3 px-2 text-center">
                                <p className="d-block ">Constant <br />
                                    <span className="text-light"> (Occurs multiple times per month.)
                                    </span>
                                </p>
                                <button className="risk-btn very-low-risk">Very Low</button>
                                </div>
                                <div className="col-3 px-2 text-center">
                                <p className="d-block ">Occasional <br />
                                    <span className="text-light"> (Occurs no more than once per year) </span>
                                </p>
                                <button className="risk-btn heigh-risk">High</button>
                                </div>
                                <div className="col-3 px-2 text-center">
                                <p className="d-block mb-0">Frequent <br />
                                    <span className="text-light"> (Occurs more
                                    than once per year,
                                    but no more than
                                    once per month)</span>
                                </p>
                                <button className="risk-btn very-heigh-risk">Very High</button>
                                </div>
                            </div>
                        </div>
                    </ReactTooltip>
                    <button className="btn-trans green-peech"><FontAwesomeIcon icon={faPencilAlt} className="mr-3"/> CREATE NEW</button>
                    </div>
                </div>
                <Questionnaire />
            </div>
        </div>
        );
    }
}
 
export default PotentialEvents;