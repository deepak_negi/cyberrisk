import { faEdit, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import React, { Component } from 'react'
import Collapsible from 'react-collapsible';
// import axios from 'axios';

interface QuestionnaireProps {
    
}
 
interface QuestionnaireState {
    surveyQuestionnaire: any;
}

const api = 'http://192.168.3.25/cyberrisk/api/SurveyQuestion';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoicGt1bWFyQHN2YW0uY29tIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZW1haWxhZGRyZXNzIjoicGt1bWFyQHN2YW0uY29tIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZ2l2ZW5uYW1lIjoiICIsIk9yZ2FuaXphdGlvbklkIjoiMiIsImp0aSI6IjdhN2VjNTUyLTA5NGQtNDk3Yy04OWE4LTBlMGU2ODIxMzU3YiIsImV4cCI6MTY0MTU1MDUwMSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1OTkyMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDIwMCJ9.zRfEJ6CaMIyA-IugIIbRiKnYqoYBjRB6ZVPu_WIIBkI'


class Questionnaire extends React.Component<QuestionnaireProps, QuestionnaireState> {
    constructor(props: QuestionnaireProps){
        super(props);

        this.state = {
            surveyQuestionnaire: []
        }

    }

    componentDidMount(){
        this.getSurveyQuestionnaire();
    }

    getSurveyQuestionnaire = () => {
        axios.get(api, { headers: {
            "Authorization": `Bearer ${token}`,
            "Access-Control-Allow-Origin": '*',
        }})
            .then(async res => {
                console.log(res)
                this.setState({
                    surveyQuestionnaire: res.data
                });
            });
    }

    handleEventDelete = (event: any) => {
        event.stopPropagation();
        event.preventDefault();
    }

    handleSectionEdit = (event: any) => {

    }

    handleSectionDelete = (event: any) => {

    }

    getCollapsibleName = (abbr: string, section: string) => {
        return (
            <span>
                {abbr}: {section}
                <span
                    className="d-none d-sm-block"
                    style={{ float: "right", marginRight: "25px" }}
                >
                    <span
                        style={{ paddingRight: "5px", cursor: "pointer" }}
                        title="Edit"
                        onClick={(e) => this.handleEventDelete(e)}
                    >
                        <FontAwesomeIcon icon={faPencilAlt} />
                    </span>
                    <span
                        style={{ paddingRight: "5px", cursor: "pointer" }}
                        title="Remove"
                        onClick={(e) => this.handleEventDelete(e)}
                    >
                        <FontAwesomeIcon icon={faTrash} />
                    </span>
                </span>
            </span>
        );
    }
    render() {
        console.log(this.state.surveyQuestionnaire)
        const {surveyQuestionnaire} = this.state;
        return (
            <div className="col-12">

                {surveyQuestionnaire.map( (question: any) => (
                    <Collapsible
                        trigger={this.getCollapsibleName(question.abbreviation, question.section)}
                        key={question.id}
                    >
                        <div className="accordion-inner p-3">
                            {question.questionModels.map((section: any) => (
                                <div key={section.id} className="card rounded shadow-sm mb-2">
                                    <div className="card-header d-flex justify-content-between">
                                        <div>
                                            <strong>{section.abbreviation}</strong>
                                            <span className="icon-gray d-inline-block">{section.question}</span>
                                        </div>
                                        <div className="text-right">
                                            <span
                                                style={{ paddingRight: "5px", cursor: "pointer" }}
                                                title="Remove"
                                                onClick={(e) => this.handleSectionEdit(e)}
                                            >
                                                <FontAwesomeIcon icon={faPencilAlt} />
                                            </span>
                                            <span
                                                style={{ paddingRight: "5px", cursor: "pointer" }}
                                                title="Remove"
                                                onClick={(e) => this.handleSectionDelete(e)}
                                            >
                                                <FontAwesomeIcon icon={faTrash} />
                                            </span>
                                        </div>
                                    </div>
                                    <div className="card-body p-3">
                                        <div className="col-4 col-md-6 d-flex align-items-center justify-content-between">
                                            {section.optionsModels.map((option: any) => (
                                                <div key={option.id} className="custom-control high custom-radio mr-4">
                                                    <label className="custom-control-label ml-2" htmlFor="exampleRadios1">
                                                        <input className="custom-control-input" type="radio" name="exampleRadios" id="exampleRadios1" defaultValue="option1" defaultChecked />
                                                        {option.label}
                                                    </label>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </Collapsible>
                ))}
                
            </div>
        );
    }
}
 
export default Questionnaire;