import React, { Component } from 'react';
import PotentialEvents from "../PotentialEvents/PotentialEvents";
import PotentialVulnerabilities from "../PotentialVulnerabilties/PotentialVulnerabilities";
import ReactTooltip from "react-tooltip";

interface TabsProps {
    
}
 
interface TabsState {
    showPotentialEvents: boolean;
    showPotentialVulnerabilities: boolean;    
}
 
class Tabs extends React.Component<TabsProps, TabsState> {
    // state = { }
    constructor(props: TabsProps){
        super(props);
        this.state = {
            showPotentialEvents: true,
            showPotentialVulnerabilities: false
        }
    }

    handleVulnerabilities = () => {
        this.setState({ showPotentialVulnerabilities: true, showPotentialEvents: false })
    }

    handleEvents = () => {
        this.setState({ showPotentialVulnerabilities: false, showPotentialEvents: true })
    }

    render() {
        const { showPotentialEvents, showPotentialVulnerabilities } = this.state;
        return (
            <>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12">
                        <ul className="nav nav-tabs px-3 mt-1" id="myTab" role="tablist">
                            <li className="nav-item" role="presentation">
                            <a className={`nav-link ${showPotentialVulnerabilities? 'active': ''} `} id="home-tab" data-toggle="tab" onClick={this.handleVulnerabilities} role="tab" aria-controls="home"
                                aria-selected="true">Potential Vulnerabilities</a>
                            </li>
                            <li className="nav-item" role="presentation">
                            <a className={`nav-link ${showPotentialEvents?  'active' : ''} `} id="profile-tab" data-toggle="tab" onClick={this.handleEvents} role="tab"
                                aria-controls="profile" aria-selected="false">Potential Events</a>
                            </li>
                        </ul>
                    </div>
                </div>

                {showPotentialVulnerabilities && <PotentialVulnerabilities />}
                {showPotentialEvents && <PotentialEvents />}
            </div>
            </>
        );
    }
}
 
export default Tabs;

